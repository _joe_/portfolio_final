import { makeStyles } from "@material-ui/core/styles";
import Box from "@material-ui/core/Box";
const defaultProps = {
  bgcolor: "white",
  borderColor: "#961e5e",
  m: 3,
  border: 5.5,
  style: { width: "50%", height: "12em" },
};
const useStyles = makeStyles((theme) => ({
  home_title: {
    display: "flex",
    justifyContent: "space-around",
    flexGrow: 1,
    fontFamily: ["Redressed", "cursive"].join(","),
    fontSize: "3.5em",
    fontWeight: 10,
  },
  home_subtitle: {
    color: "#484f4a",
    display: "flex",
    justifyContent: "space-around",
    flexGrow: 1,
    fontFamily: ["Ubuntu", "sans-serif"].join(","),
    fontSize: "1.5em",
    fontWeight: 10,
  },
}));
const Contact = () => {
  const contact_style = useStyles();
  return (
    <div
      style={{
        backgroundImage: `url(${process.env.PUBLIC_URL + "/light.jfif"})`,

        backgroundSize: "cover",
        backgroundRepeat: "no-repeat",
      }}
    >
      <Box display="flex" justifyContent="center">
        <Box borderRadius={16} {...defaultProps}>
          <br />
          <div className={contact_style.home_title}> Please Don't! </div>
          <hr
            style={
              ({ width: "100px" },
              { height: "2em" },
              { backgroundColor: "white" })
            }
          />

          <div className={contact_style.home_subtitle}>
            At least not till I edit this page...
            <br />
          </div>
        </Box>
      </Box>
      <br />
      <br />
      <br />
      <br />
      <br />
      <br />
      <br />
      <br />
      <br />
      <br />
      <br />
      <br />
      <br />
      <br />
    </div>
  );
};
export default Contact;
