import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import IconButton from "@material-ui/core/IconButton";
import { useHistory } from "react-router-dom";
import { Button } from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },

  title: {
    flexGrow: 1,
    fontFamily: ["Fraunces", "serif"].join(","),
    fontSize: "2.0em",
  },
  btn: {
    textTransform: "none",
    fontSize: "1.0em",
    color: "white",
    fontFamily: ["Fraunces", "serif"].join(","),
  },
}));

export default function Header() {
  const classes = useStyles();
  const history = useHistory();

  return (
    <div className={classes.root}>
      <AppBar
        position="static"
        style={{ background: "#440e73", boxShadow: "none" }}
      >
        <Toolbar>
          <Typography className={classes.title}>
            <Button className={classes.btn} onClick={() => history.push("/")}>
              Home
            </Button>
          </Typography>

          <div>
            <IconButton
              edge="start"
              className={classes.menuButton}
              color="inherit"
              aria-label="menu"
            >
              <Button
                className={classes.btn}
                onClick={() => history.push("/Resume")}
              >
                Resume
              </Button>
              <Button
                className={classes.btn}
                onClick={() => history.push("/projects")}
              >
                Projects
              </Button>
              <Button
                className={classes.btn}
                onClick={() => history.push("/contact")}
              >
                {" "}
                Contact{" "}
              </Button>
            </IconButton>
          </div>
        </Toolbar>
      </AppBar>
    </div>
  );
}
