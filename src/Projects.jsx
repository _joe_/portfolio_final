import React from "react";
import Card1 from "./Card1";

import { Grid } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";

const Projects = () => {
  const useStyles = makeStyles({
    gridContainer: {
      display: "flex",
      flexWrap: "wrap",
      justifyContent: "space-around",
      overflow: "hidden",
      width: "80%",
    },
    comin: {
      display: "flex",
      background: "#440e73",
      justifyContent: "space-around",
      flexGrow: 1,
      fontFamily: ["Redressed", "cursive"].join(","),
      fontSize: "2.5em",
      fontWeight: 20,
      color: "White",
      width: "100%",
      height: "60px",
    },
  });
  const classes = useStyles();
  return (
    <>
      <Grid
        container
        spacing={3}
        className={classes.gridContainer}
        justify="center"
      >
        <Grid item xs={1} sm={60} md={40}>
          <br />
          <br />
          <br />
          <Card1 />
        </Grid>
      </Grid>
      <br />
      <br />
      <br />
      <br />
      <br />
      <br />
      <div className={classes.comin}>More Projects Coming Soon!</div>
    </>
  );
};
export default Projects;
