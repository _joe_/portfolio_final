import React from "react";
import Home from "./Home";
import Contact from "./Contact";
import Resume from "./Resume";
import Projects from "./Projects";
import Header from "./Header";
import { CssBaseline } from "@material-ui/core";
import "./App.css";
import { Route, Switch } from "react-router-dom";
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles({});

export default function App() {
  const classes = useStyles();
  return (
    <div
      className={classes.container}
      style={{
        backgroundImage: `url(${process.env.PUBLIC_URL + "/light.jfif"})`,
        backgroundSize: "cover",
        backgroundRepeat: "no-repeat",
      }}
    >
      <CssBaseline />
      <Header />

      <Switch>
        <Route exact from="/" render={(props) => <Home {...props} />} />
        <Route
          exact
          path="/contact"
          render={(props) => <Contact {...props} />}
        />
        <Route exact path="/resume" render={(props) => <Resume {...props} />} />
        <Route
          exact
          path="/projects"
          render={(props) => <Projects {...props} />}
        />
      </Switch>
    </div>
  );
}
