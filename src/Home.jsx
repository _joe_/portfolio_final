import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Box from "@material-ui/core/Box";
const useStyles = makeStyles((theme) => ({
  home_title: {
    display: "flex",
    justifyContent: "space-around",
    flexGrow: 1,
    fontFamily: ["Redressed", "cursive"].join(","),
    fontSize: "3.5em",
    fontWeight: 10,
  },
  home_subtitle: {
    color: "#484f4a",
    display: "flex",
    justifyContent: "space-around",
    flexGrow: 1,
    fontFamily: ["Ubuntu", "sans-serif"].join(","),
    fontSize: "1.5em",
    fontWeight: 10,
  },
}));
const defaultProps = {
  bgcolor: "white",
  borderColor: "#961e5e",
  m: 3,
  border: 5.5,
  style: { width: "50%", height: "12em" },
};
function Home() {
  const mias = useStyles();
  return (
    <div
      style={{
        backgroundImage: `url(${process.env.PUBLIC_URL + "/light.jfif"})`,

        backgroundSize: "cover",
        backgroundRepeat: "no-repeat",
      }}
    >
      <br />
      <br />
      <br />
      <br />

      <br />

      <br />

      <br />
      <br />
      <Box display="flex" justifyContent="center">
        <Box borderRadius={16} {...defaultProps}>
          <br />
          <div className={mias.home_title}> Josephine James</div>
          <hr
            style={
              ({ width: "100px" },
              { height: "2em" },
              { backgroundColor: "white" })
            }
          />
          <div className={mias.home_subtitle}>
            Wanna-be Full Stack Developer | Currently Student
          </div>
          <br />
        </Box>
      </Box>
      <center>
        <br />
        <br />
        <br />
        <br />
        <br />

        <br />
        <br />
        <br />
        <br />
        <br />
      </center>
    </div>
  );
}
export default Home;
