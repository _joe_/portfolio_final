import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";

import Typography from "@material-ui/core/Typography";

const useStyles = makeStyles({
  root: {
    minWidth: 330,
    minHeight: 200,
  },
  bullet: {
    display: "inline-block",
    margin: "0 2px",
    transform: "scale(1.5)",
  },
  title: {
    fontSize: 30,
  },
  pos: {
    marginBottom: 12,
  },
  sub: {
    fontSize: 20,
    fontFamily: ["Ubuntu", "sans-serif"].join(","),
  },
  info: {
    fontSize: 23,
    fontFamily: ["Ubuntu", "sans-serif"].join(","),
  },
});

export default function SimpleCard() {
  const classes = useStyles();
  const bull = <span className={classes.bullet}>•</span>;

  return (
    <Card className={classes.root}>
      <CardContent>
        <Typography className={classes.sub} color="textSecondary" gutterBottom>
          Project 1
        </Typography>
        <Typography className={classes.title} variant="h4" component="h2">
          To-Do App
        </Typography>

        <hr />
        <br />
        <Typography variant="h6" component="p" className={classes.info}>
          {bull} add todo <br /> {bull} delete todo
          <br />
          {bull} complete todo
          <br />
        </Typography>
      </CardContent>
    </Card>
  );
}
